# Project Overview

Our REST API will receive requests and respond to those requests. It will allow
clients to:

- Create stores
- Create an item within a store
- Retrieve a list of all stores and their items
- Retrieve an individual store and its items given a `name`
- Retrieve only a list of items given a store `name`
