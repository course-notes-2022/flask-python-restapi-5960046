# First REST API Endpoint

Let's create our first endpoint. Modify `app.py` as follows:

```py

from flask import Flask

app = Flask(__name__) # Create a Flask app

stores = {
    "name": "My Store",
    "items": [
        {
            "name": "Chair",
            "price": 19.99
        }
    ]
}

# Create our first endpoint
@app.get('/stores')
def get_stores():
    return {"stores": stores}
```

Hit `http://127.0.0.1:5000/stores` in a browser or Postman. You should get the
following response JSON:

```json
{
  "stores": {
    "items": [
      {
        "name": "Chair",
        "price": 19.99
      }
    ],
    "name": "My Store"
  }
}
```
