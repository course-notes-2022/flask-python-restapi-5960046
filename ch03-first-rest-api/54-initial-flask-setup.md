# Initial Project Setup

- `python3.10 -m venv .venv`: Create **virtual environment**.

- `CTRL + SHIFT + P` / `CMD + SHIFT + P`: In VS Code, select Python interpreter
  (path to the `venv` you created in the previous step).

- `source .venv/bin/activate`: Activate the `.venv` environment in a terminal.

- `pip install flask`: Install Flask and dependencies

- Create an `app.py` file in your project root with the following contents:

```py
from flask import Flask

app = Flask(__name__) # Create a Flask app
```

Flask automatically looks for a file called `app.py`. This is your main server
file.

- `flask run`: Run the Flask app from the terminal window with your virtual
  environment.
