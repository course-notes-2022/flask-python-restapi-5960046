# How to Interact With and Test Your REST API

To test our project, we need to make **requests** and receive **responses**. We
can do this either manually or in an automated way.

There are various clients you can use if you wish to do manual testing:

- Insomnia
- Postman
- `curl`
- etc.
