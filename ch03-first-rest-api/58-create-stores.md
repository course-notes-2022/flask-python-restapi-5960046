# How to Create Stores in our REST API

To create a store, we will receive store data as JSON via a `POST` request.

The `POST` body will look like:

```json
{
  "name": "my-new-store"
}
```

## Application Code

`app.py`:

```py
# ...

@app.post('/store')
def create_store():
    # Get request body
    request_data = request.get_json()
    new_store = { "name": request_data["name"], "items": []}
    stores.append(new_store)
    return new_store, 201
```
