# Create Items in a Store

To create an item, the client will send:

- The `name` of the store to update
- The `item` to add to the store

The client can send the item data in a variety of ways:

- In the `body` of the request
- As part of the URL
- In the query parameters
- In the headers

`app.py`:

```py
# ...

app.post('/store/<string:name>/item')
def create_item(name):
    request_data = request.get_json()
    for store in stores:
        if store["name"] == name:
            new_item = {"name": request_data['name'], "price": request_data['price']}
            store['items'].append(new_item)
            return new_item, 201
    return {"message": "Store not found"}, 404
```
