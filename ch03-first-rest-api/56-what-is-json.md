# What is JSON?

**JSON** is a string of text that conforms to a certain **format**. Key-value
pairs can be any data type, and are separated by commas:

```json
{
  "stores": {
    "items": [
      {
        "name": "Chair",
        "price": 19.99
      }
    ],
    "name": "My Store"
  }
}
```

JSON "objects" are unordered. You can have either a **list** or an **object** at
the top-level of the object.
