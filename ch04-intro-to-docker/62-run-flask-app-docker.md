# How to Run a Flask App in a Docker Container

## Install Docker Desktop

If you are running Docker containers on MacOS or Windows, you need to install
**Docker Desktop**. Docker Desktop creates a Linux VM in which you can run
(Linux) Docker containers.

## Create Dockerfile

```Dockerfile
FROM python:3.10-alpine

EXPOSE 5000

WORKDIR /app

COPY . .

RUN pip install flask

CMD ["flask", "run", "--host", "0.0.0.0"]
```
