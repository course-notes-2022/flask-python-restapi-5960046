# What Are Docker Containers and Images?

A **virtual machine** is an emulation of an operating system. It requires a
**hypervisor**,which allows you to run other operating systems on top of your
machine's OS by provisioning **portions** of the machine's hardware for use by
the VM.

Note that there is a lot of duplication here, as the **host** OS has to interact
with hardware, as well as the **virtual** OS.

**Docker** is different. On the host server runs the Docker engine, in which
containers run **using the OS of the host** as a process. Each container has its
own storage and network.

Containers are much more lightweight and startup much faster than VMs, but you
lose the flexibility to run a different OS.

![vms versus docker](./screenshots/vm-vs-docker.png)

Most of the time, you will want to run **Linux** containers, as most
applications will be deployed to Linux servers. To run Linux containers on
Windows/MacOS, you first need to run a Linux VM. **Docker Desktop** takes care
of this for you (but it's inefficient).

## What Does a Docker Container Run?

Everything but the kernel. Any applications from the OS you want to use need to
be included in the container (e.g. Bash, curl, etc.). You also need to install
Python, `pip`, and any dependencies your app needs.

## Images

Docker containers are based on **images**. Images are a snapshot of source code,
libraries, dependencies, tools, etc. that a container needs to run (except the
OS kernel). **Dockerfiles** define images. We can use them to run one or more
containers.

![docker images](./screenshots/docker-images.png)
